open Causality

module ES = ES.Make (struct
  type label = string

  let id = None
end)

module Monad = ES.LogMessage (Monad.Cont)
open Monad

let ( let* ) = Monad.bind

module Cell = Linearisable.Cell (Monad)

type var = string

let l = Location.create ()

let emit fmt = Printf.kprintf (put l) fmt

module Buffer = struct
  type t = (var * int) list

  let write v k b = (v, k) :: b

  let read v =
    let rec aux acc = function
      | [] -> None
      | (v', k) :: rest when v = v' -> Some ((v', k) :: List.rev acc, k, rest)
      | entry :: rest -> aux (entry :: acc) rest
    in
    aux []

  let commit memory l =
    let rec aux = function
      | (var, k) :: rest ->
          let* () =
            Cell.set
              ~act:(fun () -> emit "Commit %s=%d" var k)
              (List.assoc var memory) k
          in
          aux rest
      | _ -> return ()
    in
    aux (List.rev l)
end

type reg = string

type expr = Reg of reg | Int of int | Add of expr * expr

type instr = Write of var * expr | Read of var * reg | Barrier

type thread = instr array

type program = thread list

let rec eval env = function
  | Reg var -> List.assoc var env
  | Int n -> n
  | Add (e, e') -> eval env e + eval env e'

module MS = Map.Make (String)

let eval_instr_aux memory (env, buffer, pc) = function
  | Barrier ->
      let* () = Buffer.commit memory buffer in
      let* () = emit "barrier" in
      return (env, [], pc + 1)
  | Write (var, e) ->
      let value = eval env e in
      let buffer = Buffer.write var value buffer in
      let* () = emit "Local write: %s := %d" var value in
      return (env, buffer, pc + 1)
  | Read (var, reg) -> (
    match Buffer.read var buffer with
    | None ->
        let* n =
          Cell.get
            ~act:(fun value -> emit "External read of %s %s=%d" var reg value)
            (List.assoc var memory)
        in
        return ((reg, n) :: env, buffer, pc + 1)
    | Some (before, value, after) ->
        let local_read =
          let* () = emit "Local read of %s %s=%d" var reg value in
          return ((reg, value) :: env, buffer, pc + 1)
        and memory_read =
          let* () = Buffer.commit memory before in
          let* value =
            Cell.get
              ~act:(fun value ->
                emit "External read of %s(+commit) %s=%d" var reg value )
              (List.assoc var memory)
          in
          return ((reg, value) :: env, after, pc + 1)
        in
        sum [local_read; memory_read] )

let eval_instr memory (env, buffer, pc) i =
  eval_instr_aux memory (env, buffer, pc) i

let eval_thread memory program =
  let rec aux (env, buffer, pc) =
    if pc = Array.length program then Buffer.commit memory buffer
    else
      let* state = eval_instr memory (env, buffer, pc) program.(pc) in
      aux state
  in
  aux ([], [], 0)

let eval_program memory threads =
  join_list (List.map (eval_thread memory) threads)

let rec values_of = function
  | [] -> return []
  | (var, v) :: rest ->
      let* value = Cell.get v in
      values_of rest >>= fun l -> return ((var, value) :: l)

let run vars program =
  Monad.results
  @@
  let memory =
    List.map (fun v -> (v, Cell.create ~show:Int.to_string ~name:v 0)) vars
  in
  add_filter (Domain.Message.Filter.location l)
  >> eval_program memory program
  >>= fun _ -> return ()

let p1 = [[|Write ("x", Int 1); Write ("x", Int 2)|]]

let buff =
  [ [|Write ("x", Int 1); Read ("y", "r")|]
  ; [|Write ("y", Int 1); Read ("x", "s")|] ]

let buff' =
  [ [|Write ("x", Int 1); Barrier; Read ("y", "r")|]
  ; [|Write ("y", Int 1); Barrier; Read ("x", "s")|] ]

let buff'' =
  [ [| Write ("x", Int 1)
     ; Read ("x", "rr")
     ; Read ("y", "r")
     ; Write ("z", Reg "r") |]
  ; [| Write ("y", Int 1)
     ; Read ("y", "ss")
     ; Read ("x", "s")
     ; Write ("w", Reg "s") |]
  ; [| Write ("y", Int 1)
     ; Read ("y", "ss")
     ; Read ("x", "s")
     ; Write ("w", Reg "s") |] ]
